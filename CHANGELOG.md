# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2022-11-25)


### Features

* **ci:** Removed alpha branches ([fccb8b1](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/fccb8b199dbb3bbcd4b303be224cf32ed6057d98))
* README.md ([115befd](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/115befd1a9ca1ad4646d5f2faf9fc00b3d7f4deb))
* Rewrite ([12d0025](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/12d00252d8e55f7ff9d5216b79eef71ec241e45e))


### Bug Fixes

* Bot responds to any at name but not its own one ([228f2aa](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/228f2aae65d464ca989b0df5afbc433dd9a535e6))
* **ci:** Deployment job name ([bc67e1f](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/bc67e1f62e581b0e07c169d19fbc2c53f9fcdf71))
* **ci:** Docker authentication ([d3d6646](https://git.abfelbaum.dev/abfelbaum/bots/telegram/atadminbot/commit/d3d6646b5a5ea2bc665c5b9264432090c5c38a7c))
