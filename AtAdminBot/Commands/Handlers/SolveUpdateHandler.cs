using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace AtAdminBot.Commands.Handlers;

public class SolveUpdateHandler : ITelegramUpdate<CallbackQuery>
{
    private readonly ITelegramBotClient _telegram;
    private readonly ICachedTelegramBotClient _cachedTelegram;
    private readonly ILogger<SolveUpdateHandler> _logger;
    private readonly IStringLocalizer<SolveUpdateHandler> _localizer;

    public SolveUpdateHandler(ITelegramBotClient telegram, ICachedTelegramBotClient cachedTelegram, ILogger<SolveUpdateHandler> logger, IStringLocalizer<SolveUpdateHandler> localizer)
    {
        _telegram = telegram;
        _cachedTelegram = cachedTelegram;
        _logger = logger;
        _localizer = localizer;
    }

    public async Task InvokeAsync(CallbackQuery callback, CancellationToken cancellationToken = new())
    {
        switch (callback.Data)
        {
            case "solve":
                await Solve(callback, cancellationToken);
                break;

            case "solved_violation":
            case "solved_tolerated":
            case "solved_no_violation":
            case "solved_other":
                await Solved(callback, cancellationToken);
                break;

            default:
                return;
        }
    }

    private async Task Solve(CallbackQuery callback, CancellationToken cancellationToken)
    {
        if (!await IsAdminQuery(callback, cancellationToken))
        {
            return;
        }

        if (callback.Message is not { } callbackMessage)
        {
            return;
        }

        var inlineKeyboard = new InlineKeyboardMarkup(new[]
        {
            [InlineKeyboardButton.WithCallbackData(_localizer["button.violation"], "solved_violation")],
            [InlineKeyboardButton.WithCallbackData(_localizer["button.tolerated"], "solved_tolerated")],
            [InlineKeyboardButton.WithCallbackData(_localizer["button.no.violation"], "solved_no_violation")],
            new[]
            {
                InlineKeyboardButton.WithCallbackData(_localizer["button.other"], "solved_other")
            }
        });

        try
        {
            await _telegram.MakeRequestAsync(new EditMessageReplyMarkupRequest
            {
                ChatId = callbackMessage.Chat.Id,
                MessageId = callbackMessage.MessageId,
                ReplyMarkup = inlineKeyboard
            }, cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Warning, e, "Cannot edit solve message for {ChatTitle} ({ChatId})",
                callbackMessage.Chat.Title, callbackMessage.Chat.Id);
        }
    }

    private async Task Solved(CallbackQuery callbackQuery, CancellationToken cancellationToken)
    {
        if (!await IsAdminQuery(callbackQuery, cancellationToken))
        {
            await _telegram.MakeRequestAsync(new AnswerCallbackQueryRequest
            {
                CallbackQueryId = callbackQuery.Id,
                Text = _localizer["YouNeedToBeAnAdmin"],
                ShowAlert = true
            }, cancellationToken: cancellationToken);

            return;
        }

        if (callbackQuery.Data is null || callbackQuery.Message is not { } callbackMessage)
        {
            return;
        }


        string text = string.Format(_localizer["SolvedBy"], Html(GetName(callbackQuery.From)),
            GetSolvedText(callbackQuery.Data));

        try
        {
            await _telegram.MakeRequestAsync(new EditMessageTextRequest
            {
                ChatId = callbackMessage.Chat.Id,
                MessageId = callbackMessage.MessageId,
                Text = text,
                ParseMode = ParseMode.Html,
                ReplyMarkup = null
            }, cancellationToken: cancellationToken);
        }
        catch
        {
            // ignored
        }
    }

    private async Task<bool> IsAdminQuery(CallbackQuery callbackQuery, CancellationToken cancellationToken = new())
    {
        if (callbackQuery.Message is not { } callbackQueryMessage)
        {
            return false;
        }

        ChatMember[] admins = await _cachedTelegram.MakeRequestAsync(new GetChatAdministratorsRequest
        {
            ChatId = callbackQueryMessage.Chat.Id
        }, cancellationToken: cancellationToken);

        if (admins.Any(x => x.User.Id == callbackQuery.From.Id))
        {
            return true;
        }

        return false;
    }

    private string GetName(User user)
    {
        return user.Username is null ? $"{user.FirstName} {user.LastName}" : $"@{user.Username}";
    }

    private string Html(string html)
    {
        return WebUtility.HtmlEncode(html);
    }

    private string GetSolvedText(string solvedId)
    {
        return solvedId switch
        {
            "solved_violation" => _localizer["solved.violation"],
            "solved_tolerated" => _localizer["solved.tolerated"],
            "solved_no_violation" => _localizer["solved.no.violation"],
            "solved_other" => _localizer["solved.other"],
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}