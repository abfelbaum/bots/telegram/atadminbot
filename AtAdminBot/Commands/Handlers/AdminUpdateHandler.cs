using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using AtAdminBot.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AtAdminBot.Commands.Handlers;

public class AdminUpdateHandler : ITelegramUpdate<Message>
{
    private readonly ICachedTelegramBotClient _cachedTelegram;
    private readonly IAdministratorNotificationService _notificationService;

    public AdminUpdateHandler(ICachedTelegramBotClient cachedTelegram, IAdministratorNotificationService notificationService)
    {
        _cachedTelegram = cachedTelegram;
        _notificationService = notificationService;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        if (message.Chat.Type is not (ChatType.Group or ChatType.Supergroup))
        {
            return;
        }

        if (message.Text is null)
        {
            return;
        }

        string[] args = message.Text.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);


        string? command = args.FirstOrDefault()?.ToLowerInvariant();

        if (command is null)
        {
            return;
        }

        if (!command.StartsWith('@'))
        {
            return;
        }

        if (command != "@admin" && command != "@admins")
        {
            User me = await _cachedTelegram.MakeRequestAsync(new GetMeRequest(), cancellationToken: cancellationToken);

            if (!command.Equals($"@{me.Username}", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }
        }

        await _notificationService.Notify(message, cancellationToken);
    }
}