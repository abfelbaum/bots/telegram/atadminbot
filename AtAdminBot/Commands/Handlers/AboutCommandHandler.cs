using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Microsoft.Extensions.Localization;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AtAdminBot.Commands.Handlers;

public class AboutCommandHandler : ITelegramUpdate<Message>
{
    private readonly ITelegramBotClient _telegram;
    private readonly IStringLocalizer<AboutCommandHandler> _localizer;

    public AboutCommandHandler(ITelegramBotClient telegram, IStringLocalizer<AboutCommandHandler> localizer)
    {
        _telegram = telegram;
        _localizer = localizer;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        string text = _localizer["AboutText"];

        await _telegram.MakeRequestAsync(new SendMessageRequest
        {
            ChatId = message.Chat.Id,
            Text = text,
            ParseMode = ParseMode.Markdown,
            ReplyParameters = new ReplyParameters
            {
                MessageId = message.MessageId
            }
        }, cancellationToken: cancellationToken);
    }
}