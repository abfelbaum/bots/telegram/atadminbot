﻿using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using AtAdminBot.Interfaces;
using Telegram.Bot.Types;

namespace AtAdminBot.Commands.Handlers;

public class AdminCommandHandler : ITelegramUpdate<Message>
{
    private readonly IAdministratorNotificationService _notificationService;

    public AdminCommandHandler(IAdministratorNotificationService notificationService)
    {
        _notificationService = notificationService;
    }

    public async Task InvokeAsync(Message message, CancellationToken cancellationToken = new())
    {
        await _notificationService.Notify(message, cancellationToken);
    }
}