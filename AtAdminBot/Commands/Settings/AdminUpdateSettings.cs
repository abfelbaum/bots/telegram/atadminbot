using System;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.Updates;
using AtAdminBot.Commands.Handlers;
using Telegram.Bot.Types;

namespace AtAdminBot.Commands.Settings;

public class AdminUpdateSettings : MessageUpdate
{
    protected override Task<Type?> GetUpdateHandlerType(Update update)
    {
        return Task.FromResult(typeof(AdminUpdateHandler))!;
    }
}