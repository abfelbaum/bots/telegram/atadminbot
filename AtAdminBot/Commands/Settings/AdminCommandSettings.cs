﻿using System.Collections.Generic;
using System.Globalization;
using Abfelbaum.Telegram.Bot.Framework.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Abstractions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Types;
using AtAdminBot.Commands.Handlers;
using Microsoft.Extensions.Localization;

namespace AtAdminBot.Commands.Settings;

public sealed class AdminCommandSettings : TelegramCommand
{
    public AdminCommandSettings(IBotService botService, IScopeMatchingService scopeMatchingService,
        IStringLocalizer<AdminCommandSettings> localizer) : base(botService, scopeMatchingService)
    {
        Documentation = new CommandDocumentation
        {
            ShortDescription = "command.description.short",
            LongDescription = "command.description.long",
            Name = "command.name",
            Localizer = localizer,
            CultureInfos = { CultureInfo.GetCultureInfo("en"), CultureInfo.GetCultureInfo("de") },
            Aliases = { "command.alias.report" },
            Usages = { "command.usage.admin", "command.usage.report" }
        };
    }

    public override CommandDocumentation Documentation { get; init; }

    public override IEnumerable<CommandHandler> Handlers { get; } =
        new[]
        {
            new CommandHandler<AdminCommandHandler>(CommandScope.AllGroupChats())
        };
}