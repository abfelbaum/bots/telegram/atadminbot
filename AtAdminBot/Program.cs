﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using Abfelbaum.Telegram.Bot.Framework.Commands.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Commands.Help.Extensions;
using Abfelbaum.Telegram.Bot.Framework.ConcurrentUpdates.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Configuration;
using Abfelbaum.Telegram.Bot.Framework.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Hosting;
using Abfelbaum.Telegram.Bot.Framework.Polling;
using Abfelbaum.Telegram.Bot.Framework.RateLimiting;
using Abfelbaum.Telegram.Bot.Framework.RequestCulture.Extensions;
using Abfelbaum.Telegram.Bot.Framework.Types;
using AtAdminBot.Commands.Settings;
using AtAdminBot.Interfaces;
using AtAdminBot.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types.Enums;

TelegramApplicationBuilder<DefaultContext> builder = TelegramApplication<DefaultContext>.CreateBuilder(new TelegramApplicationOptions
{
    Args = args,
    ApplicationName = "AtAdminBot"
});
// builder.Services.AddTransient<HttpMessageHandlerBuilder, CustomHttpMessageHandlerBuilder>();

builder.Host.UseConsoleLifetime();

builder.Logging.AddFilter("System.Net.Http.HttpClient", LogLevel.Warning);

builder.Configuration.AddCommandLine(args);
builder.Configuration.AddEnvironmentVariables("AtAdminBot_");
builder.Configuration.AddJsonFile("appsettings.json", true);
builder.Configuration.AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", true);

builder.Services.Configure<BotOptions>(builder.Configuration.GetSection("Bot"));
builder.Services.AddBot(builder.Configuration.GetSection("Bot"));

builder.AddCachedClient();

builder.Services.AddPolling();

builder.Services.AddCommandUpdater();

builder.Services.AddConcurrentUpdates();
builder.Services.AddRequestScheduler();
builder.Services.AddRequestCulture(options =>
{
    options.DefaultCulture = CultureInfo.GetCultureInfo("en");
});

builder.AddCommand<AboutCommandSettings>();
builder.AddCommand<AdminCommandSettings>();
builder.AddUpdate<AdminUpdateSettings>(UpdateType.Message);
builder.AddUpdate<SolveUpdateSettings>(UpdateType.CallbackQuery);


builder.AddHelpCommand();

builder.Services.AddTransient<IAdministratorNotificationService, AdministratorNotificationService>();

TelegramApplication<DefaultContext> app = builder.Build();

app.UseRequestCulture();
app.UseConcurrentUpdates();
app.UseUpdateHandling();

await app.RunAsync();


/// <inheritdoc />
public class CustomHttpMessageHandlerBuilder : HttpMessageHandlerBuilder
{
    /// <summary>
    /// </summary>
    public override string Name { get; set; } = null!;

    /// <summary>
    /// </summary>
    public override HttpMessageHandler PrimaryHandler { get; set; } = null!;

    /// <inheritdoc />
    public override IList<DelegatingHandler> AdditionalHandlers => new List<DelegatingHandler>();

    // Our custom builder doesn't care about any of the above.
    /// <inheritdoc />
    public override HttpMessageHandler Build()
    {
        return new HttpClientHandler
        {
            Proxy = new WebProxy
            {
                Address = new Uri("http://localhost:8000"),
                BypassProxyOnLocal = false,
                UseDefaultCredentials = false
            },
            ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
        };
    }
}
