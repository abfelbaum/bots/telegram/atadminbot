﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Abfelbaum.Telegram.Bot.Framework.CachedClient;
using AtAdminBot.Interfaces;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Requests;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace AtAdminBot.Services;

public class AdministratorNotificationService : IAdministratorNotificationService
{
    private readonly ITelegramBotClient _telegram;
    private readonly IStringLocalizer<AdministratorNotificationService> _localizer;
    private readonly ILogger<AdministratorNotificationService> _logger;
    private readonly ICachedTelegramBotClient _cachedTelegram;

    public AdministratorNotificationService(ITelegramBotClient telegram,
        IStringLocalizer<AdministratorNotificationService> localizer, ILogger<AdministratorNotificationService> logger,
        ICachedTelegramBotClient cachedTelegram)
    {
        _telegram = telegram;
        _localizer = localizer;
        _logger = logger;
        _cachedTelegram = cachedTelegram;
    }

    public async Task Notify(Message message, CancellationToken cancellationToken = new())
    {
        ChatMember[] administrators = await _cachedTelegram.MakeRequestAsync(new GetChatAdministratorsRequest
        {
            ChatId = message.Chat.Id
        }, cancellationToken: cancellationToken);

        List<ChatMember> administratorsWithoutBots = administrators.Where(x => !x.User.IsBot).ToList();

        Message? notifyMessage = await NotifyUser(message, administratorsWithoutBots.Count, cancellationToken);

        int successfulNotifications = await NotifyAdministrators(message, administratorsWithoutBots, cancellationToken);

        await SendSolveButton(message, notifyMessage, successfulNotifications, cancellationToken);
    }

    private async Task<Message?> NotifyUser(Message message, int count, CancellationToken cancellationToken = new())
    {
        try
        {
            return await _telegram.MakeRequestAsync(new SendMessageRequest
            {
                ChatId = message.Chat.Id,
                Text = string.Format(_localizer["TryingToNotify"], count,
                    _localizer["Admin" + (count != 1 ? "Plural" : "Singular")]),
                ReplyParameters = new ReplyParameters
                {
                    MessageId = message.MessageId
                }
            }, cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Warning, e, "Cannot send message to {ChatTitle} ({ChatId})", message.Chat.Title,
                message.Chat.Id);
        }

        return null;
    }

    private async Task<int> NotifyAdministrators(Message message, List<ChatMember> administrators,
        CancellationToken cancellationToken = new())
    {
        IEnumerable<Task<Message>> notificationTasks =
            from administrator in administrators
            where !administrator.User.IsBot
            let notificationText = BuildAdminNotification(message, administrator.User.LanguageCode)
            select _telegram.MakeRequestAsync(new SendMessageRequest
            {
                ChatId = administrator.User.Id,
                Text = notificationText,
                ParseMode = ParseMode.Html
            }, cancellationToken: cancellationToken);

        var successfulNotifications = 0;
        foreach (Task<Message> task in notificationTasks)
        {
            try
            {
                await task;
                successfulNotifications++;
            }
            catch
            {
                // ignored
            }
        }

        return successfulNotifications;
    }

    private async Task SendSolveButton(Message message, Message? notifyMessage, int successfulNotifications,
        CancellationToken cancellationToken)
    {
        if (notifyMessage is null)
        {
            return;
        }

        var inlineKeyboardMarkup = new InlineKeyboardMarkup(new[]
        {
            new[]
            {
                InlineKeyboardButton.WithCallbackData(_localizer["button.solve"], "solve")
            }
        });

        try
        {
            await _telegram.MakeRequestAsync(new EditMessageTextRequest
            {
                ChatId = message.Chat.Id,
                MessageId = notifyMessage.MessageId,
                Text = string.Format(_localizer["AdminsNotified"], successfulNotifications,
                    _localizer["Admin" + (successfulNotifications != 1 ? "Plural" : "Singular")]),
                ReplyMarkup = inlineKeyboardMarkup
            }, cancellationToken: cancellationToken);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Warning, e, "Cannot edit message at {ChatTitle} ({ChatId})", message.Chat.Title,
                message.Chat.Id);
        }
    }


    private string GetMessageLink(Message message)
    {
        return $"{GetChatLink(message.Chat)}{message.MessageId}";
    }

    private string GetChatLink(Chat chat)
    {
        return $"https://t.me/{chat.Username ?? $"c/{Math.Abs(chat.Id).ToString()[3..]}"}/";
    }

    private string? Html(string? html)
    {
        return WebUtility.HtmlEncode(html);
    }

    private string? GetName(User? user)
    {
        if (user is null)
        {
            return null;
        }

        return user.Username is null ? $"{user.FirstName} {user.LastName}" : $"@{user.Username}";
    }

    private string BuildAdminNotification(Message message, string? languageCode)
    {
        CultureInfo currentCulture = CultureInfo.CurrentCulture;
        CultureInfo currentUiCulture = CultureInfo.CurrentUICulture;

        if (languageCode is not null)
        {
            var cultureInfo = CultureInfo.GetCultureInfo(languageCode);
            CultureInfo.CurrentCulture = cultureInfo;
            CultureInfo.CurrentUICulture = cultureInfo;
        }

        bool isReply = message.ReplyToMessage?.From is not null;

        string template = isReply
            ? _localizer["notification.withreply"]
            : _localizer["notification.withoutreply"];


        string chatName = $"""<a href="{GetMessageLink(message)}">{Html(message.Chat.Title)}</a>""";

        string? senderName = GetName(message.From);
        string? reportedName =  GetName(message.ReplyToMessage?.From);
        string? reportedText = message.ReplyToMessage?.Text ?? message.Text;

        CultureInfo.CurrentCulture = currentCulture;
        CultureInfo.CurrentUICulture = currentUiCulture;

        return string.Format(template, chatName, senderName, reportedText, reportedName);
    }
}