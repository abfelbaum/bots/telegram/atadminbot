﻿using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace AtAdminBot.Interfaces;

public interface IAdministratorNotificationService
{
    public Task Notify(Message message, CancellationToken cancellationToken = new());
}