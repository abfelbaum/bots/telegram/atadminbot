# Getting started

First you have to [install](./install) the bot.

After a successful install the steps should be the same to the [normal getting started page](../guides/getting-started.md), except for the bots name.