---
description: "How to react to and resolve reports and configure the bot."
---

# Administrators

In addition to using the [commands intended for users](users.md#commands) chat administrators have some commands they
can use to customize the bot and react to reports.

## Start the bot

:::info

Since Telegram requires users to start a bot before the bot is able to send them any messages in a private chat this
step is required for any chat administrator that wants to receive notifications.

:::

To start the bot you just have to [open the private chat](https://t.me/atadminbot) with the bots user and press the
start button.

## React to a report

When you receive a notification from the bot the message will contain some information about the report. In some cases a
user may even provide a specific description about the reason why they notified you.

On reading the message you will notice the link to the group in the message text. This link will lead you to the
specific message that created the report.

:::info

In case the jumping to the message does not work for you have a look at the [troubleshooting section](#the-link-does-not-lead-me-to-the-right-message).

:::

When you found that message it will, if not already resolved by another administrator, look like this report:

import Image from '@theme/IdealImage';

<Image img={require('../../assets/showcase.png')} />

When you press the _Solve_ button you will be presented with the following options:

:::info

These options are just general options that can be handled individually in each chat. Please have a talk to the other
chat administrators to find a general policy for your chat.

:::

| Name         | Idea                                                                                                                                      |
|--------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| Violation    | When a user reported a rule violation and the administrator took consequences (i.e. deleting the message, banning or warning the user..). |
| Tolerated    | Sometimes a rule violation happens but there is no need to take aktion on that.                                                           |
| No Violation | Got a false report? This option got you!                                                                                                  |
| Other        | Everything that does not fit in the other options.                                                                                        |

:::caution

When one option is chosen the action can not be reversed!

:::

## Troubleshooting

### The bot does not notify me

You most likely are either not an administrator in the chat or did not [start the bot](#start-the-bot).

:::info

Updating the administrator status of a user can take up to 10 minutes due to caching.

:::

### The link does not lead me to the right message

Some Telegram clients do not support jumping to messages based on message ids. That means that you will have to search
the message by yourself.

I would do that by:

1. Look when the notification was sent by the bot.
2. Find that time in the group chat.

:::info

When the bot is under heavy load the notification might not be sent immediately. If you cannot find the message at the
specified time maybe it was sent a few minutes earlier. When you still cannot find that message then it might have been
deleted.

:::