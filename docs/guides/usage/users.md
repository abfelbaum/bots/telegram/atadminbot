---
sidebar_position: 1
description: "How to notify administrators."
---

# Users

As a groups member the bot has one basic functionality for you: Notify administrators!

Please refer to your groups rules when you want to know when exactly you should use this functionality. Some groups may
have specific rules for that. To get a general idea on what that might be here are some use cases:

* Spam
* Some really important stuff
* Other rule violations

## Commands

### `@admin`

To notify the chats administrators you just need to tag one of the following usernames at the start of your message: `@admin`, `@admins`, `@atadminbot`.

The bot will recognize that message and try to notify the chat administrators of the group.

You may add additional details to your report by replying to the message you want to report or adding a reason after the tag.

#### Examples

##### Simple

> ```
> @admin
> ```

> ```
> Hey, an admin is needed in Bot Test Chat by @Abfelbaum: @admin
> ```

##### With description

> ```
> @admin Oh no there is an evil spammer here!
> ```

> ```
> Hey, an admin is needed in Bot Test Chat by @Abfelbaum: @admin Oh no there is an evil spammer here!
> ```

##### With reply

> In reply to another message that says "I am an evil spammer!"
> ```
> @admin
> ```

> ```
> Hey, an admin is needed in Bot Test Chat by @Abfelbaum because of @Abfelbaum: I am an evil spammer!
> ```